
import './App.css';
import {Route, BrowserRouter, Routes} from 'react-router-dom';
import {createBrowserHistory} from 'history';
import {NotFound} from './pages/errors/NotFound';
import {LoginPage} from './pages/login/LoginPage';
import {HomePage} from './pages/home/HomePage';
import {RegisterPage} from './pages/register/RegisterPage';
import {ProfilePage} from './pages/profile/ProfilePage';
import {Unauthorized} from './pages/errors/Unauthorized';
import TeamForm from './features/team/pages/TeamForm';
import AllTeams from './features/team/pages/AllTeams';
import AllUsers from './features/user/pages/AllUsers';
import TestPage from './features/Examples/TestPage';
import TeamRoleAdd from './features/role/pages/TeamRoleAdd';
import AllTeamRoles from './features/role/pages/AllTeamRoles';
import TeamEdit from './features/team/pages/TeamEdit';
import TeamRoleEdit from './features/role/pages/TeamRoleEdit';


function App() {


  return (
    <div className="App">
      <BrowserRouter history={createBrowserHistory()}>
          <Routes>
              <Route exact path="/" element={<LoginPage />} />
              <Route exact path="/test" element={<TestPage />} />
              <Route exact path="/login" element={<LoginPage />} />
              <Route exact path="/register" element={<RegisterPage />} />
              <Route exact path="/home" element={<HomePage />} />
              <Route exact path="/teams" element={<TeamForm />} />
              <Route exact path="/teams/editteam" element={<TeamEdit />} />
              <Route exact path="/teams/allteams" element={<AllTeams />} />
              <Route exact path="/users/allusers" element={<AllUsers />} />
              <Route exact path="/teamroles/addteamrole" element={<TeamRoleAdd />} />
              <Route exact path="/teamroles/editteamrole" element={<TeamRoleEdit />} />
              <Route exact path="/teamroles/allteamroles" element={<AllTeamRoles />} />
              <Route exact path="/users/allusers" element={<AllUsers />} />
              <Route exact path="/profile" element={<ProfilePage />} />
              <Route exact path="/404" element={<NotFound />} />
              <Route exact path="/401" element={<Unauthorized />} />
          </Routes>

      </BrowserRouter>

    </div>
  );
}

export default App;
