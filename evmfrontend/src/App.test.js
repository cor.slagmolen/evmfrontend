import {render, screen, cleanup, fireEvent} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from './App';
import {LoginPage} from './pages/login/LoginPage';

afterEach(cleanup);

describe('login screen behavior', () => {
    test('of sign is successful', async () => {
        const user = userEvent.setup();
        const {container} = render(<LoginPage/>);
        // logRoles(container);
        const signinButton = screen.getByRole('button', {name: 'Sign in'});
        const inputUsername = screen.getByLabelText('username');
        fireEvent.change(inputUsername, {target: {value: 'evmuser'}});
        expect(inputUsername.value).toBe('evmuser');
        const inputUserpassword = screen.getByLabelText('password');
        fireEvent.change(inputUserpassword, {target: {value: 'password123'}});
        expect(inputUserpassword.value).toBe('password123');

        expect(signinButton).toBeEnabled();
        await user.click(signinButton);
        expect(screen.getByText('Authenticated successful')).toBeInTheDocument();
    });
    test('of sign is unsuccessful', () => {
        const {container} = render(<LoginPage/>);
        // logRoles(container);
        const signinButton = screen.getByRole('button', {name: 'Sign in'});
        const inputUsername = screen.getByLabelText('username');
        fireEvent.change(inputUsername, {target: {value: 'evmuser'}});
        expect(inputUsername.value).toBe('evmuser');
        const inputUserpassword = screen.getByLabelText('password');
        fireEvent.change(inputUserpassword, {target: {value: 'password'}});
        expect(inputUserpassword.value).toBe('password');

        expect(signinButton).toBeEnabled();
        fireEvent.click(signinButton);
        expect(screen.getByText('Not Authenticated successful')).toBeInTheDocument();
    });
});
