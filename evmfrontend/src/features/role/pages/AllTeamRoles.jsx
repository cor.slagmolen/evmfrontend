import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Header from '../../../pages/header_Footer/Header';
import {
    MDBBadge,
    MDBBtn,
    MDBContainer,
    MDBPagination,
    MDBPaginationItem, MDBPaginationLink,
    MDBTable,
    MDBTableBody,
    MDBTableHead
} from 'mdb-react-ui-kit';
import {useDispatch, useSelector} from 'react-redux';
import Footer from '../../../pages/header_Footer/Footer';
import {useNavigate} from 'react-router-dom';
import {setTeamRoleId} from '../store/TeamRoleSlice';

export default function AllTeamRoles() {

    const token = useSelector((state) => state.auth.token);
    const user = useSelector((state) => state.auth.loginUser);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const getData = async () => {

        const {data} = await axios.get('http://localhost:8080/api/teamrole/allteamroles', {
            headers: {
                Authorization: 'Bearer ' + token
            }
        });
        setData(data);
    };
    useEffect(() => {
        getData();
    }, []);

    const handleEditTeamRole = (id) => {

        dispatch(setTeamRoleId(id));
        console.log(id);
        navigate('/teamroles/editteamrole');
    };
    return (
        <div>
            <Header/>
            <MDBContainer className="p-3 my-5 d-flex flex-column w-80">
                <MDBTable striped
                          bordered
                          hover
                          entriesOptions={[5, 20, 25]}
                          entries={5}
                          pagesAmount={4}
                          align='middle'
                >
                    <MDBTableHead className="table-primary">
                        <tr>
                            <th>Team Role</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </MDBTableHead>
                    <MDBTableBody>
                        {data.map((d) => (
                            <tr>
                                <td>
                                    <div className='d-flex align-items-center'>
                                        <div className=' ms-3 d-flex align-items-center'>
                                            <p className='fw-bold mb-0'>{d.name}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div className='d-flex align-items-center'>
                                        <div className=' ms-3 d-flex align-items-center'>
                                            <p className='mb-0'>{d.description}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <MDBBtn color='link' rounded size='sm' onClick={e => handleEditTeamRole(d.id)}>
                                        Edit
                                    </MDBBtn>
                                </td>
                            </tr>
                        ))
                        }
                    </MDBTableBody>
                </MDBTable>
                <MDBPagination className='mb-0'>
                    <MDBPaginationItem>
                        <MDBPaginationLink href='#'>Previous</MDBPaginationLink>
                    </MDBPaginationItem>
                    <MDBPaginationItem>
                        <MDBPaginationLink href='#'>1</MDBPaginationLink>
                    </MDBPaginationItem>
                    <MDBPaginationItem>
                        <MDBPaginationLink href='#'>2</MDBPaginationLink>
                    </MDBPaginationItem>
                    <MDBPaginationItem>
                        <MDBPaginationLink href='#'>3</MDBPaginationLink>
                    </MDBPaginationItem>
                    <MDBPaginationItem>
                        <MDBPaginationLink href='#'>Next</MDBPaginationLink>
                    </MDBPaginationItem>
                </MDBPagination>
            </MDBContainer>
            <Footer/>
        </div>
    );
}
