import Header from '../../../pages/header_Footer/Header';
import React, {useState} from 'react';
import {MDBBtn, MDBCard, MDBCardBody, MDBCheckbox, MDBContainer, MDBInput, MDBTextArea} from 'mdb-react-ui-kit';
import {OverlayTrigger, Popover} from 'react-bootstrap';
import {MDBBox, MDBBtnGroup} from 'mdbreact';
import {useSelector} from 'react-redux';
import axios from 'axios';
import Footer from '../../../pages/header_Footer/Footer';


export default function TeamRoleAdd() {
    const [roleName, setRoleName] = useState('');
    const [roleDescription, setRoleDescription] = useState('');

    const token = useSelector((state) => state.auth.token);
    const user = useSelector((state) => state.auth.loginUser);

    function handleTeamRoleChange(event) {
        setRoleName(event.target.value);
    }
    function handleTeamRoleDescChange(event) {
        setRoleDescription(event.target.value);
    }


    function handleCreateTeamRole(event) {
        const teamRole = {
            name: roleName,
            description: roleDescription
        };
        const headers = {
            Authorization: 'Bearer '+ token
        };
        const addTeamRole = async () => {

            const {data} = await axios.post('http://localhost:8080/api/teamrole/add', teamRole, {headers});
        };
        addTeamRole();
    }
    const handleClear = (e) => {
        setRoleName('');
        setRoleDescription('');
    };


    const popoverTopTeamRole = (
        <Popover id="popover-positioned-top" title="Popover top">
            <Popover.Header as="h3">Team role</Popover.Header>
            <Popover.Body>
                Like...developer..Business Analist, Tester etc!.
            </Popover.Body>
        </Popover>
    );
    const popoverTopTeamRoleDesc = (
        <Popover id="popover-positioned-top" title="Popover top">
            <Popover.Header as="h3">Team role Description</Popover.Header>
            <Popover.Body>
                Describe the role in more detail.
            </Popover.Body>
        </Popover>
    );


    return (
        <div>
            <Header/>
            <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
                <h2 className="text-center"><a href="#!">Create new team role</a></h2>
                <MDBCard className='my-5 cascading-right'
                         style={{background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)'}}>
                    <MDBCardBody className='p-5 shadow-5 align-text-top'>
                        <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={popoverTopTeamRole}>
                            <MDBInput wrapperClass='mb-3 text-primary' aria-label='teamrole' label='Team role'
                                      id='teamrole' value={roleName || ''}
                                      onChange={handleTeamRoleChange} type='text'/>
                        </OverlayTrigger>
                        <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={popoverTopTeamRoleDesc}>
                            <MDBTextArea wrapperClass='mb-3 text-primary' aria-label='teamroledescription' label='Team role description'
                                      id='teamroledecs' value={roleDescription || ''}
                                      onChange={handleTeamRoleDescChange} type='text'/>
                        </OverlayTrigger>
                        <div className="d-flex justify-content-between mx-4 mb-4">
                            <MDBCheckbox name='flexCheck' value='' id='flexCheckDefault' label='Assign to team members'/>
                            <MDBBtn className="btn btn-info btn-sm rounded-5" id="Cancel-btn" onClick={handleClear}>Clear</MDBBtn>
                            <a href="/teamroles/allteamroles">Back</a>
                        </div>


                        <MDBBtn className="mb-4 w-100" id="CreateTeamRole-btn" onClick={handleCreateTeamRole}>Create new
                            team role</MDBBtn>
                    </MDBCardBody>
                </MDBCard>
            </MDBContainer>
            <Footer/>
        </div>
    );

}