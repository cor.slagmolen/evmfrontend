import Header from '../../../pages/header_Footer/Header';
import React, {useEffect, useState} from 'react';
import {MDBBtn, MDBCard, MDBCardBody, MDBCheckbox, MDBContainer, MDBInput, MDBTextArea} from 'mdb-react-ui-kit';
import {OverlayTrigger, Popover} from 'react-bootstrap';
import {MDBBox, MDBBtnGroup} from 'mdbreact';
import {useSelector} from 'react-redux';
import axios from 'axios';
import Footer from '../../../pages/header_Footer/Footer';
import {useNavigate} from 'react-router-dom';


export default function TeamRoleEdit({id}) {
    const [roleName, setRoleName] = useState('');
    const [teamRole, setTeamRole] = useState({});
    const [changedTeamRole, setChangedTeamRole] = useState({});
    const [roleDescription, setRoleDescription] = useState('');

    const teamRoleSelected = useSelector((state) => state.teamRole.teamRoleId);
    const token = useSelector((state) => state.auth.token);
    const user = useSelector((state) => state.auth.loginUser);
    const navigate = useNavigate();

      const headers = {
        Authorization: 'Bearer ' + token
    };
    const getTeamRole = async () => {
        await axios.get('http://localhost:8080/api/teamrole/' + teamRoleSelected, {headers})
            .then((res) => {
                setTeamRole(teamRole => ({
                    ...teamRole,...res.data
                }  ));
                setChangedTeamRole(teamRole);
            }).catch((err) => console.log(err));
    };


    useEffect(() => {
        getTeamRole();
        console.log('team role is' + teamRole.name);

    },[teamRole.id]);
    function handleTeamRoleChange(event) {
        setRoleName(event.target.value);
    }
    function handleTeamRoleDescChange(event) {
        setRoleDescription(event.target.value);
    }
    const handleRoleNameChange = (e) => {
        let updatedValue ={};
        updatedValue= {'name' : e.target.value};
        setChangedTeamRole(changedTeamRole=> ({
            ...changedTeamRole,...updatedValue
        }  ));
    };
    const handleRoleDescChange = (e) => {
        let updatedValue ={};
        updatedValue= {'description' : e.target.value};
        setChangedTeamRole(changedTeamRole=> ({
            ...changedTeamRole,...updatedValue
        }  ));
    };


    function handleEditTeamRole(event) {
        const headers = {
            Authorization: 'Bearer ' + token
        };
        const editTeamRole = async () => {

            const {data} = await axios.put('http://localhost:8080/api/teamrole/edit', changedTeamRole, {headers});
        };
        editTeamRole();
        navigate('/teamroles/allteamroles');

    }
    const handleCancel = (e) => {
        setChangedTeamRole(teamRole);
    };


    const popoverTopTeamRole = (
        <Popover id="popover-positioned-top" title="Popover top">
            <Popover.Header as="h3">Team role</Popover.Header>
            <Popover.Body>
                Like...developer..Business Analist, Tester etc!.
            </Popover.Body>
        </Popover>
    );
    const popoverTopTeamRoleDesc = (
        <Popover id="popover-positioned-top" title="Popover top">
            <Popover.Header as="h3">Team role Description</Popover.Header>
            <Popover.Body>
                Describe the role in more detail.
            </Popover.Body>
        </Popover>
    );


    return (
        <div>
            <Header/>
            <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
                <h2 className="text-center"><a href="#!">Edit team role</a></h2>
                <MDBCard className='my-5 cascading-right'
                         style={{background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)'}}>
                    <MDBCardBody className='p-5 shadow-5 align-text-top'>
                        <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={popoverTopTeamRole}>
                            <MDBInput wrapperClass='mb-3 text-primary' aria-label='teamrole' label='Team role'
                                      id='teamrole' value={changedTeamRole.name || ''}
                                      onChange={handleRoleNameChange} type='text'/>
                        </OverlayTrigger>
                        <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={popoverTopTeamRoleDesc}>
                            <MDBTextArea wrapperClass='mb-3 text-primary' aria-label='teamroledescription' label='Team role description'
                                      id='teamroledecs' value={changedTeamRole.description || ''}
                                      onChange={handleRoleDescChange} type='text'/>
                        </OverlayTrigger>
                        <div className="d-flex justify-content-between mx-4 mb-4">
                            <MDBCheckbox name='flexCheck' value='' id='flexCheckDefault' label='Assign to team members'/>
                            <MDBBtn className="btn btn-info btn-sm rounded-5" id="Cancel-btn" onClick={handleCancel}>Cancel</MDBBtn>
                            <a href="/teamroles/allteamroles">Back</a>
                        </div>


                        <MDBBtn className="mb-4 w-100" id="CreateTeamRole-btn" onClick={handleEditTeamRole}>Save Team role</MDBBtn>
                    </MDBCardBody>
                </MDBCard>
            </MDBContainer>
            <Footer/>
        </div>
    );

}