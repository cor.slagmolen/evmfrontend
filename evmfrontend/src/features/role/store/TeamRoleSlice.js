import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    teamSelected : ''
};
const teamRoleSlice = createSlice({
    name: 'teamrole',
    initialState,
    reducers: {
        setTeamRoleId(state, action) {
            state.teamRoleId= action.payload;
        }
    }
});
export const {setTeamRoleId} = teamRoleSlice.actions;
export default teamRoleSlice.reducer;