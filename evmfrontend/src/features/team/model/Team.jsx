export class Team{
    constructor(
        id,
        teamname,
        teampurpose,
        organization
    )
    {
        this.id = id;
        this.teamname = teamname;
        this.teampurpose = teampurpose;
        this.organization = organization;
    }
}