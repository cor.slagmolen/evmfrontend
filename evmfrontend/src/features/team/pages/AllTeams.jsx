import React, {useState, useEffect} from 'react';
import axios from 'axios';
import TeamCard from './TeamCard';
import {Team} from '../model/Team';
import Header from '../../../pages/header_Footer/Header';
import {MDBContainer} from 'mdb-react-ui-kit';
import {useSelector} from 'react-redux';
import Footer from '../../../pages/header_Footer/Footer';

export default function AllTeams() {

    const token = useSelector((state) => state.auth.token);
    const user = useSelector((state) => state.auth.loginUser);
    const [data, setData] = useState([]);
    const getData = async () => {

        const {data} = await axios.get('http://localhost:8080/api/team/allteams', {
            headers: {
                Authorization: 'Bearer ' + token
            }
        });
        setData(data);
    };
    useEffect(() => {
        getData();
    }, []);
    console.log(data);
    return (
        <div>
            <Header/>
            <MDBContainer className="p-3 my-3 d-flex flex-column w-80">
                <p className='fw-bold fs-5 text-primary'>All Teams</p>
                <div className='row mx-6'>
                    {data.map((e) => (
                        <div className="col-sm-4" >
                            <TeamCard id={e.id} teamName={e.teamName} teamPurpose={e.teamPurpose} organization={e.organization}
                                      teamType={e.teamType}/>
                        </div>
                    ))
                    }
                </div>

            </MDBContainer>
            <Footer/>
        </div>
    );
}
