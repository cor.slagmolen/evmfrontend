import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import store from '../../../store';
import {
    MDBCard,
    MDBCardBody,
    MDBCardTitle,
    MDBCardText,
    MDBBtn, MDBCardHeader, MDBCardFooter
} from 'mdb-react-ui-kit';
import {setUser} from '../../user/store/UserSlice';
import {setTeamId} from '../store/TeamSlice';
import {redirect, useNavigate} from 'react-router-dom';




export default function TeamCard({id, teamName, teamPurpose, organization, teamType}) {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const handleEditTeam = (e) => {
        dispatch(setTeamId(id));
        console.log(id);
        navigate('/teams/editteam');
    };
    return (
        <MDBCard className='shadow'>
            <MDBCardHeader className='bg-primary text-white   '>{teamName}</MDBCardHeader>
            <MDBCardBody className='bg-info-subtle text-primary'>
                <MDBCardTitle>{teamType}</MDBCardTitle>
                    <MDBCardText>
                        Purpose : {teamPurpose}
                    </MDBCardText>
                <MDBCardText>
                    Organization: {organization}
                </MDBCardText>
            </MDBCardBody>
            <MDBCardFooter className='d-flex bg-primary'>
                <MDBBtn className='btn btn-sm rounded-3 mx-3 bg-info text-white' rounded outline color='success' onClick={handleEditTeam}>Edit</MDBBtn>
                <MDBBtn className='btn btn-sm rounded-3 bg-info text-white' rounded outline color='success'>Show team members</MDBBtn>
            </MDBCardFooter>
        </MDBCard>
    );
}