import Header from '../../../pages/header_Footer/Header';
import React, {useEffect, useState} from 'react';
import {MDBBtn, MDBCard, MDBCardBody, MDBCheckbox, MDBContainer, MDBInput} from 'mdb-react-ui-kit';
import {OverlayTrigger, Popover} from 'react-bootstrap';
import {useSelector} from 'react-redux';
import axios from 'axios';
import {useNavigate} from 'react-router-dom';


export default function TeamEdit() {

    const teamSelected = useSelector((state) => state.team.teamId);
    const token = useSelector((state) => state.auth.token);
    const user = useSelector((state) => state.auth.loginUser);
    const navigate = useNavigate();
    const [team, setTeam] = useState({});
    const [changedTeam, setChangedTeam] = useState({});

    const headers = {
        Authorization: 'Bearer ' + token
    };
    /*const fetchData = () => {
        axios.get('http://localhost:8080/api/team/' + teamSelected, {headers})
            .then(ret => {
            console.log({ ret });
            setTeam(ret.data);
        }).catch(e => {console.log(e);});
    };
    useEffect(() => {
        fetchData();
    }, []); */
    const getTeam = async () => {
        await axios.get('http://localhost:8080/api/team/' + teamSelected, {headers})
            .then((res) => {
                setTeam(team=> ({
                    ...team,...res.data
                }  ));
                setChangedTeam(team);
                console.log(res.data);
            }).catch((err) => console.log(err));
    };


    useEffect(() => {
        getTeam();
        console.log('team is' + team.teamName);
        updateTeamType(team.teamType);
    },[team.id]);


    function updateTeamType(type) {
        if (type === 'Feature') {
            document.getElementById('featureTeamCheck').checked = true;
            document.getElementById('platformTeamCheck').checked = false;
            document.getElementById('componentTeamCheck').checked = false;
        }
        if (type === 'Platform') {
            document.getElementById('featureTeamCheck').checked = false;
            document.getElementById('platformTeamCheck').checked = true;
            document.getElementById('componentTeamCheck').checked = false;
        }
        if (type === 'Component') {
            document.getElementById('featureTeamCheck').checked = false;
            document.getElementById('platformTeamCheck').checked = false;
            document.getElementById('componentTeamCheck').checked = true;
        }
    }
    const handleTypeChange = (e) => {
        let updatedValue ={};
        updatedValue= {'teamType' : e.target.value};
        setChangedTeam(changedTeam=> ({
            ...changedTeam,...updatedValue
        }  ));
        let type = e.target.value;

        if (type === 'Feature') {
            document.getElementById('featureTeamCheck').checked = true;
            document.getElementById('platformTeamCheck').checked = false;
            document.getElementById('componentTeamCheck').checked = false;
        }
        if (type === 'Platform') {
            document.getElementById('featureTeamCheck').checked = false;
            document.getElementById('platformTeamCheck').checked = true;
            document.getElementById('componentTeamCheck').checked = false;
        }
        if (type === 'Component') {
            document.getElementById('featureTeamCheck').checked = false;
            document.getElementById('platformTeamCheck').checked = false;
            document.getElementById('componentTeamCheck').checked = true;
        }

    };


    function handleEditTeam(event) {

        const headers = {
            Authorization: 'Bearer ' + token
        };
        const editTeam = async () => {

            const {data} = await axios.put('http://localhost:8080/api/team/edit', changedTeam, {headers});
        };
        editTeam();
        navigate('/teams/allteams');

    }
    /*const handleNameChange = (e) => {
        setChangedTeam(...changedTeam, {teamName: e.target.value});
    };*/


    const popoverTopTeamname = (
        <Popover id="popover-positioned-top" title="Popover top">
            <Popover.Header as="h3">Amazing Team name</Popover.Header>
            <Popover.Body>
                Think of an <strong>amazing</strong> team name!.
            </Popover.Body>
        </Popover>
    );
    const popoverTopTeampurpose = (
        <Popover id="popover-positioned-top" title="Popover top">
            <Popover.Header as="h3">Purpose of the team</Popover.Header>
            <Popover.Body>
                What is the <strong>added value</strong> of your team to the organization!.
            </Popover.Body>
        </Popover>
    );

    const handleNameChange = (e) => {
        let updatedValue ={};
        updatedValue= {'teamName' : e.target.value};
        setChangedTeam(changedTeam=> ({
            ...changedTeam,...updatedValue
        }  ));
    };
    const handlePurposeChange = (e) => {
        let updatedValue ={};
        updatedValue= {'teamPurpose' : e.target.value};
        setChangedTeam(changedTeam=> ({
            ...changedTeam,...updatedValue
        }  ));
    };
    const handleOrganizationChange = (e) => {
        let updatedValue ={};
        updatedValue= {'organization' : e.target.value};
        setChangedTeam(changedTeam=> ({
            ...changedTeam,...updatedValue
        }  ));
    };
    const handleCancel = (e) => {
        setChangedTeam(team);
        updateTeamType(team.teamType);
    };


    return (
        <div>
            <Header/>
            <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
                <h2 className="text-center"><a href="#!">Edit team</a></h2>
                <MDBCard className='my-5 cascading-right' id='teamform'
                         style={{background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)'}}>
                    <MDBCardBody className='p-5 shadow-5 align-text-top'>
                        <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={popoverTopTeamname}>
                            <MDBInput wrapperClass='mb-3 text-primary' aria-label='teamname' label='Team name'
                                      id='teamname' value={changedTeam.teamName} onChange={handleNameChange}
                                      type='text'/>
                        </OverlayTrigger>
                        <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={popoverTopTeampurpose}>
                            <MDBInput wrapperClass='mb-3 text-primary' aria-label='teampurpose' label='Team purpose'
                                      id='teampurpose' value={changedTeam.teamPurpose} onChange={handlePurposeChange}
                                      type='text'/>
                        </OverlayTrigger>
                        <MDBInput wrapperClass='mb-3 text-primary' aria-label='organization' label='Organization'
                                  id='organization' value={changedTeam.organization} onChange={handleOrganizationChange}
                                  type='text'/>

                        <div className="d-flex justify-content-sm-around  mx-2 mb-5">
                            <MDBCheckbox name='featureTeamCheck' value='Feature' id='featureTeamCheck'
                                         label='Feature Team' onChange={handleTypeChange}/>
                            <MDBCheckbox name='componentTeamCheck' value='Component' id='componentTeamCheck'
                                         label='Component Team' onChange={handleTypeChange}/>
                            <MDBCheckbox name='platformTeamCheck' value='Platform' id='platformTeamCheck'
                                         label='Platform Team' onChange={handleTypeChange}/>
                        </div>
                        <div className="d-flex justify-content-between mx-4 mb-4">
                            <MDBCheckbox name='flexCheck' value='' id='flexCheckDefault' label='Add team members'/>
                            <MDBBtn className="btn btn-info btn-sm rounded-5" id="Cancel-btn" onClick={handleCancel}>Cancel</MDBBtn>
                            <a href="/teams/allteams">Back</a>
                        </div>

                        <MDBBtn className="mb-4 w-100" id="CreateTeam-btn" onClick={handleEditTeam}>Save
                            team</MDBBtn>
                    </MDBCardBody>
                </MDBCard>
            </MDBContainer>
        </div>
    );
}