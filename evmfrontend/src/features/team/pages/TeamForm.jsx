import Header from '../../../pages/header_Footer/Header';
import React, {useState} from 'react';
import {MDBBtn, MDBCard, MDBCardBody, MDBCheckbox, MDBContainer, MDBInput} from 'mdb-react-ui-kit';
import {OverlayTrigger, Popover} from 'react-bootstrap';
import {MDBBox, MDBBtnGroup} from 'mdbreact';
import {useSelector} from 'react-redux';
import axios from 'axios';


export default function TeamForm() {
    const [teamName, setTeamName] = useState('');
    const [teamPurpose, setTeamPurpose] = useState('');
    const [organization, setOrganization] = useState('');
    const [teamType, setTeamType] = useState('');
    const token = useSelector((state) => state.auth.token);
    const user = useSelector((state) => state.auth.loginUser);
    const team = useSelector((state) => state.team.teamSelected);

    function handleTeamNameChange(event) {
        setTeamName(event.target.value);
    }

    function handleTeamPurposeChange(event) {
        setTeamPurpose(event.target.value);
    }

    function handleOrganizationChange(event) {
        setOrganization(event.target.value);
    }

    function handleCreateTeam(event) {
        const team = {
            teamName: teamName,
            teamPurpose: teamPurpose,
            teamType: teamType,
            organization: organization
        };
        const headers = {
            Authorization: 'Bearer '+ token
        };
        const addTeam = async () => {

            const {data} = await axios.post('http://localhost:8080/api/team/add', team, {headers});
        };
        addTeam();
        resetForm();


    }
    const resetForm = () => {
        setTeamName('');
        setTeamPurpose('');
        setTeamType('');
        setOrganization('');
    };
    function handleTeamType(e) {
        if (e.target.id === 'featureTeamCheck') {
            setTeamType('Feature');
            document.getElementById('componenTeamCheck').checked = false;
            document.getElementById('platformTeamCheck').checked = false;
        }
        if (e.target.id === 'componenTeamCheck') {
            setTeamType('Feature');
            document.getElementById('featureTeamCheck').checked = false;
            document.getElementById('platformTeamCheck').checked = false;
        }
        if (e.target.id === 'platformTeamCheck') {
            setTeamType('Feature');
            document.getElementById('featureTeamCheck').checked = false;
            document.getElementById('componenTeamCheck').checked = false;
        }

    }

    const popoverTopTeamname = (
        <Popover id="popover-positioned-top" title="Popover top">
            <Popover.Header as="h3">Amazing Team name</Popover.Header>
            <Popover.Body>
                Think of an <strong>amazing</strong> team name!.
            </Popover.Body>
        </Popover>
    );
    const popoverTopTeampurpose = (
        <Popover id="popover-positioned-top" title="Popover top">
            <Popover.Header as="h3">Purpose of the team</Popover.Header>
            <Popover.Body>
                What is the <strong>added value</strong> of your team to the organization!.
            </Popover.Body>
        </Popover>
    );


    return (
        <div>
            <Header/>
            <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
                <h2 className="text-center"><a href="#!">Create new team</a></h2>
                <MDBCard className='my-5 cascading-right' id='teamform'
                         style={{background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)'}}>
                    <MDBCardBody className='p-5 shadow-5 align-text-top'>
                        <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={popoverTopTeamname}>
                            <MDBInput wrapperClass='mb-3 text-primary' aria-label='teamname' label='Team name'
                                      id='teamname' value={teamName || ''}
                                      onChange={handleTeamNameChange} type='text'/>
                        </OverlayTrigger>
                        <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={popoverTopTeampurpose}>
                            <MDBInput wrapperClass='mb-3 text-primary' aria-label='teampurpose' label='Team purpose'
                                      id='teampurpose'
                                      value={teamPurpose || ''}
                                      onChange={handleTeamPurposeChange} type='text'/>
                        </OverlayTrigger>
                        <MDBInput wrapperClass='mb-3 text-primary' aria-label='organization' label='Organization'
                                  id='organization' value={organization || ''}
                                  onChange={handleOrganizationChange} type='text'/>

                        <div className="d-flex justify-content-sm-around  mx-2 mb-5">
                                <MDBCheckbox name='featureTeamCheck' value='featureTeamCheck' id='featureTeamCheck'
                                             label='Feature Team' onChange={handleTeamType} />
                                <MDBCheckbox name='componentTeamCheck' value='componenTeamCheck' id='componenTeamCheck'
                                             label='Component Team' onChange={handleTeamType}/>
                                <MDBCheckbox name='platformTeamCheck' value='platformTeamCheck' id='platformTeamCheck'
                                             label='Platform Team' onChange={handleTeamType}/>
                        </div>
                        <div className="d-flex justify-content-between mx-4 mb-4">
                            <MDBCheckbox name='flexCheck' value='' id='flexCheckDefault' label='Add team members'/>
                            <a href="!#">Back</a>
                        </div>

                        <MDBBtn className="mb-4 w-100" id="CreateTeam-btn" onClick={handleCreateTeam}>Create new
                            team</MDBBtn>
                    </MDBCardBody>
                </MDBCard>
            </MDBContainer>
        </div>
    );

}