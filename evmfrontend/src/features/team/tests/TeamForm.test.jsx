import {render, screen, fireEvent} from '@testing-library/react';
import TeamForm from '../pages/TeamForm';
import userEvent from '@testing-library/user-event';

describe('teamform tests', () => {
    test('if right fields exists', () => {
        const { container} = render(<TeamForm/>);
        const teamname = screen.getByLabelText('teamname');
        expect(teamname).toBeInTheDocument();
        const teampurpose = screen.getByLabelText('teampurpose');
        expect(teampurpose).toBeInTheDocument();
        const organization = screen.getByLabelText('organization');
        expect(organization).toBeInTheDocument();
    });
    test('if hover over teamname message appears', async () => {
        const user = userEvent.setup();
        const {container} = render(<TeamForm/>);
        const nopopup = screen.queryByText(/amazing team name/i);
        expect(nopopup).not.toBeInTheDocument();
        // Start hover
        const teamname = screen.getByLabelText('teamname');
        expect(teamname).toBeInTheDocument();
        await user.hover(teamname);
        const popup = screen.getByText(/amazing team name/i);
        expect(popup).toBeInTheDocument();
        // start unhover
        await user.unhover(teamname);
        const nopopup2 = screen.queryByText(/amazing team name/i);
        expect(nopopup2).not.toBeInTheDocument();


    });
    test('if hover over teampurpose message appears', async () => {
        const user = userEvent.setup();
        const {container} = render(<TeamForm/>);
        const nopopup = screen.queryByText(/purpose of the team/i);
        expect(nopopup).not.toBeInTheDocument();
        // Start hover
        const teampurpose = screen.getByLabelText('teampurpose');
        expect(teampurpose).toBeInTheDocument();
        await user.hover(teampurpose);
        const popup = screen.getByText(/purpose of the team/i);
        expect(popup).toBeInTheDocument();
        // start unhover
        await user.unhover(teampurpose);
        const nopopup2 = screen.queryByText(/purpose of the team/i);
        expect(nopopup2).not.toBeInTheDocument();

    });

    
});