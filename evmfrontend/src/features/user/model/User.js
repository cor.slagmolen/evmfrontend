export class User{
    constructor(
        id,
        firstname,
        lastname,
        username,
        password,
        role
    )
    {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.role = role;
    }
}