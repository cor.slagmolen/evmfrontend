import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Header from '../../../pages/header_Footer/Header';
import {MDBBadge, MDBBtn, MDBContainer, MDBTable, MDBTableBody, MDBTableHead} from 'mdb-react-ui-kit';
import {useSelector} from 'react-redux';
import Footer from '../../../pages/header_Footer/Footer';

export default function AllUser() {

    const token = useSelector((state) => state.auth.token);
    const user = useSelector((state) => state.auth.loginUser);
    const [data, setData] = useState([]);
    const getData = async () => {

        const {data} = await axios.get('http://localhost:8080/api/admin/allusers', {
            headers: {
                Authorization: 'Bearer ' + token
            }
        });
        setData(data);
    };
    useEffect(() => {
        getData();
    }, []);
    console.log(data);
    return (
        <div>
            <Header/>
            <MDBContainer className="p-3 my-5 d-flex flex-column w-80">
                <MDBTable striped
                          bordered
                          hover
                          entriesOptions={[5, 20, 25]}
                          entries={5}
                          pagesAmount={4}
                          align='middle'
                          >
                    <MDBTableHead className="table-primary">
                        <tr>
                            <th>User</th>
                            <th>Team Role</th>
                            <th>Status</th>
                            <th>Position</th>
                            <th>Actions</th>
                        </tr>
                    </MDBTableHead>
                    <MDBTableBody>
                        {data.map((e) => (
                            <tr>
                                <td>
                                    <div className='d-flex align-items-center'>
                                        <img
                                            src='https://mdbootstrap.com/img/new/avatars/8.jpg'
                                            alt=''
                                            style={{ width: '45px', height: '45px' }}
                                            className='rounded-circle'
                                        />
                                        <div className=' ms-3 d-flex align-items-center'>
                                            <p className='fw-bold mb-0'>{e.username}</p>
                                            <p className='ms-3 text-muted mb-0'>{e.email}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p className='fw-normal mb-1'>Software engineer</p>
                                    <p className='text-muted mb-0'>IT department</p>
                                </td>
                                <td>
                                    <MDBBadge color='success' pill>
                                        Active
                                    </MDBBadge>
                                </td>
                                <td>Senior</td>
                                <td>
                                    <MDBBtn color='link' rounded size='sm'>
                                        Edit
                                    </MDBBtn>
                                    <MDBBtn color='link' rounded size='sm'>
                                        Show details
                                    </MDBBtn>
                                </td>
                            </tr>
                        ))
                        }
                    </MDBTableBody>
                </MDBTable>
            </MDBContainer>
            <Footer/>
        </div>
    );
}
