import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    loginUser : '',
    token : ''
};
const userSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setUser(state, action) {

            state.loginUser = action.payload;
        },
        setToken(state, action) {
            state.token = action.payload;
        }
    }
});
export const {setUser, setToken} = userSlice.actions;
export default userSlice.reducer;
