import {rest} from 'msw';

export const handlers = [
    rest.get('http://localhost:3030/teams', (req, res, ctx) => {
        return res(
            ctx.json([
                {teamname: 'Top team', teampurpose: 'test team', organization: 'leaseplan', componenteam:false, featureteam: true, platformteam: false},
                {teamname: 'BLS team', teampurpose: 'calculation team', organization: 'leaseplan', componenteam:true, featureteam: false, platformteam: false},
            ])
        ); 
    })
];
