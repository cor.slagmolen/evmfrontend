import {Link} from 'react-router-dom';

function NotFound() {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12 text-center">
                    <span className="display-1">
                        404
                    </span>
                    <div className="ab-4 lead">
                        Oops! We can't seem to find the page you are looking for.
                    </div>
                    <Link className="btn btn-link" to="/login">Back to Home</Link>
                </div>
            </div>
        </div>

    );
}
export {NotFound};
