import {Link} from 'react-router-dom';

function Unauthorized() {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12 text-center">
                    <span className="display-1">
                        401
                    </span>
                    <div className="ab-4 lead">
                        Oops! You are not authorized to see that page.
                    </div>
                    <Link className="btn btn-link" to="/login">Back to Home</Link>
                </div>
            </div>
        </div>
    );
}
export {Unauthorized};