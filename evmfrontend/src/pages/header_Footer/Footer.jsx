import React from 'react';
import { MDBFooter } from 'mdb-react-ui-kit';

export default function Footer() {

    return (
        <div id="gfg" className='fixed-bottom'>


            <MDBFooter color="primary" bgColor='light'
                       className='text-center text-lg-left'>
                <div className="text-center p-3 background-color: rgba(0, 0, 0, 0.2);">
                    <h6>Earned Value Management ---The Agile way of tracking costs and benefits.</h6>
                    © 2023 Copyright:
                    <a className="text-secondary" href="http://www.hyparxis.nl"> Hyparxis Automatisering Bv</a>
                </div>

            </MDBFooter>
        </div>
    );
}