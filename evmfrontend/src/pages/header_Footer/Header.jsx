import React, { useState } from 'react';
import {redirect, useNavigate} from 'react-router-dom';
import {Link} from 'react-router-dom';
import {
    MDBContainer,
    MDBNavbar,
    MDBNavbarBrand,
    MDBNavbarToggler,
    MDBIcon,
    MDBNavbarNav,
    MDBNavbarItem,
    MDBNavbarLink,
    MDBBtn,
    MDBDropdown,
    MDBDropdownToggle,
    MDBDropdownMenu,
    MDBDropdownItem,
    MDBCollapse,
    MDBInputGroup
} from 'mdb-react-ui-kit';

export default function Header() {
    const [showBasic, setShowBasic] = useState(false);
    const navigate = useNavigate();

    const handleAllteams = () => {
        navigate('/teams/allteams');

    };
    const handleAddTeam = () => {
        navigate('/teams');

    };
    const handleAllUsers = () => {
        navigate('/users/allusers');

    };
    const handleAddRoletoUser = () => {
        navigate('/users/addrole');

    };
    const handleAddTeamRole = () => {
        navigate('/teamroles/addteamrole');

    };
    const handleAddUserRole = () => {
        navigate('/userroles/adduserrole');

    };
    const handleListUserRoles = () => {
        navigate('/userroles/alluserroles');

    };
    const handleListTeamRoles = () => {
        navigate('/teamroles/allteamroles');

    };

    return (
        <MDBNavbar expand='lg' className='navbar navbar-dark bg-primary'>
            <MDBContainer fluid>
                <MDBNavbarBrand href='#'>Brand</MDBNavbarBrand>

                <MDBNavbarToggler
                    aria-controls='navbarSupportedContent'
                    aria-expanded='false'
                    aria-label='Toggle navigation'
                    onClick={() => setShowBasic(!showBasic)}
                >
                    <MDBIcon icon='bars' fas />
                </MDBNavbarToggler>

                <MDBCollapse navbar show={showBasic}>
                    <MDBNavbarNav className='mr-auto mb-2 mb-lg-0'>
                        <MDBNavbarItem>
                            <MDBNavbarLink active aria-current='page' href='/test'>
                                Home
                            </MDBNavbarLink>
                        </MDBNavbarItem>
                        <MDBNavbarItem>
                            <MDBDropdown>
                                <MDBDropdownToggle tag='a' className='nav-link' role='button'>
                                    Role
                                </MDBDropdownToggle>
                                <MDBDropdownMenu>
                                    <MDBDropdownItem link onClick={handleAddUserRole}>Add User role</MDBDropdownItem>
                                    <MDBDropdownItem link onClick={handleAddTeamRole}>Add Team role</MDBDropdownItem>
                                    <MDBDropdownItem link onClick={handleListUserRoles}>List User roles</MDBDropdownItem>
                                    <MDBDropdownItem link onClick={handleListTeamRoles}>List Team roles</MDBDropdownItem>
                                </MDBDropdownMenu>
                            </MDBDropdown>
                        </MDBNavbarItem>
                        <MDBNavbarItem>
                            <MDBDropdown>
                                <MDBDropdownToggle tag='a' className='nav-link' role='button'>
                                    User
                                </MDBDropdownToggle>
                                <MDBDropdownMenu>
                                    <MDBDropdownItem link onClick={handleAddRoletoUser}>Add Role</MDBDropdownItem>
                                    <MDBDropdownItem link onClick={handleAllUsers}>List all User</MDBDropdownItem>
                                </MDBDropdownMenu>
                            </MDBDropdown>
                        </MDBNavbarItem>
                        <MDBNavbarItem>
                            <MDBDropdown>
                                <MDBDropdownToggle tag='a' className='nav-link' role='button'>
                                    Team
                                </MDBDropdownToggle>
                                <MDBDropdownMenu>
                                    <MDBDropdownItem link onClick={handleAddTeam}>Create Team</MDBDropdownItem>
                                    <MDBDropdownItem link onClick={handleAllteams}>List all teams</MDBDropdownItem>
                                </MDBDropdownMenu>
                            </MDBDropdown>
                        </MDBNavbarItem>
                        <MDBNavbarItem>
                            <MDBNavbarLink  disabled href='#' tabIndex={-1} aria-disabled='true'>
                                Disabled
                            </MDBNavbarLink>
                        </MDBNavbarItem>
                    </MDBNavbarNav>
                        <MDBNavbarItem>
                            <MDBInputGroup tag="form" className='justify-content-end mb-4'>
                                <input className='form-control' placeholder="Type query" aria-label="Search" type='Search' />
                                <MDBBtn>Search</MDBBtn>
                            </MDBInputGroup>
                        </MDBNavbarItem>
                </MDBCollapse>
            </MDBContainer>
        </MDBNavbar>
    );
}