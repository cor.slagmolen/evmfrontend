import React, {useEffect, useRef, useState} from 'react';
import {
    MDBContainer,
    MDBCard,
    MDBCardBody,
    MDBTabs,
    MDBTabsItem,
    MDBTabsLink,
    MDBTabsContent,
    MDBTabsPane,
    MDBBtn,
    MDBInput,
    MDBCheckbox,
    MDBIcon,
}
    from 'mdb-react-ui-kit';
import Header from '../header_Footer/Header';
import Footer from '../header_Footer/Footer';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import store from '../../store';
import {setToken, setUser} from '../../features/user/store/UserSlice';

function LoginPage() {
    const userRef = useRef(null);
    const errRef = useRef();
    const [username, setUsername] = useState('evmuser');
    const [firstname, setFirstname] = useState('  ');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState(' ');
    const [justifyActive, setJustifyActive] = useState('tab1');
    const [authenticated, setAuthenticated] = useState(false);
    const [errMsg, setErrMsg] = useState('');

    const dispatch = useDispatch();
    const customer = useSelector((user) => user.auth.loginUser);
    const token = useSelector((user) => user.auth.Token);


    useEffect(() => {
        // eslint-disable-next-line no-unused-expressions
        userRef.current.focus;

    }, []);
    useEffect(() => {
        setErrMsg('');
    }, [username, password]);


    function handleUsernameChange(event) {
        setUsername(event.target.value);
    }

    function handleFirstnameChange(event) {
        setFirstname(event.target.value);
    }

    function handleLastnameChange(event) {
        setLastname(event.target.value);
    }

    function handleEmailChange(event) {
        setEmail(event.target.value);
    }

    function handlePasswordChange(event) {
        setPassword(event.target.value);
    }


    const handleSignup = (e) => {
        // Prevent the default submit and page reload
        e.preventDefault();

        // Handle validations
        axios
            .post('http://localhost:8080/api/auth/signup', {username, firstname, email, lastname, password})
            .then(response => {
                if (response.data.status === 1) {
                    console.log(username + ' succesfully registrated at:' + new Date().toLocaleTimeString());
                    setAuthenticated(false);
                    dispatch(setUser(''));
                } else {
                    setAuthenticated(false);
                    dispatch(setUser(''));
                    setErrMsg(response.data);
                    console.log(username + ' tried to register at:' + new Date().toLocaleTimeString());
                }
            })
            .catch((error) => {
                if (error.response) {
                    //request was made and server responded with an error
                    console.log(error.response.status);
                    setAuthenticated(false);
                    dispatch(setUser(''));
                    setErrMsg('Registration not succesfull!!' + error.response.data.message);
                    console.log(username + ' tried to register at:' + new Date().toLocaleTimeString() + error.response.data.message);

                } else if (error.request) {
                    //request was made but no response received
                    console.log(error.request);
                    setAuthenticated(false);
                    dispatch(setUser(''));
                    setErrMsg('Registration not succesfull!!  Server did not respond...');
                    console.log(username + ' tried to register at:' + new Date().toLocaleTimeString() + 'Server did not respond');

                } else {
                    console.log('Error:', error.message);

                }
            });


    };
    const handleSignin = (e) => {

        // Prevent the default submit and page reload
        e.preventDefault();

        // Handle validations
        axios
            .post('http://localhost:8080/api/auth/login', {username, password})
            .then(response => {
                if (response.data.Token) {
                    const token = response.data.Token;
                    dispatch(setToken(token));
                    setAuthenticated(true);
                    dispatch(setUser('Cor'));
                    console.log(username + ' succesfully login at:' + new Date().toLocaleTimeString());
                } else {
                    setAuthenticated(false);
                    dispatch(setUser(''));
                    setErrMsg('Login not succesfull!!  Try again.');
                    console.log(username + ' tried to login at:' + new Date().toLocaleTimeString());
                }
            })
            .catch((error) => {
                console.log(error);
                setAuthenticated(false);
                dispatch(setUser(''));
                setErrMsg('Login not succesfull!!  Try again.');
                console.log(username + ' tried to login at:' + new Date().toLocaleTimeString());
            });
        ;


    };

    const handleJustifyClick = (value) => {
        if (value === justifyActive) {
            return;
        }

        setJustifyActive(value);
    };
    return (
        <div>

            <Header/>
            <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
                <section>
                    <p ref={errRef} className={errMsg ? 'errmsg' : 'offscreen'} aria-live="assertive">{errMsg}</p>
                </section>
                {authenticated === true && <h2>Authenticated successful</h2>}
                <MDBCard className='my-5 cascading-right'
                         style={{background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)'}}>
                    <MDBCardBody className='p-5 shadow-5 text-center'>


                        <MDBTabs pills justify className='mb-3 d-flex flex-row justify-content-between'>
                            <MDBTabsItem>
                                <MDBTabsLink onClick={() => handleJustifyClick('tab1')}
                                             active={justifyActive === 'tab1'}>
                                    Login
                                </MDBTabsLink>
                            </MDBTabsItem>
                            <MDBTabsItem>
                                <MDBTabsLink onClick={() => handleJustifyClick('tab2')}
                                             active={justifyActive === 'tab2'}>
                                    Register
                                </MDBTabsLink>
                            </MDBTabsItem>
                        </MDBTabs>

                        <MDBTabsContent>

                            <MDBTabsPane show={justifyActive === 'tab1'}>

                                <div className="text-center mb-3">
                                    <p>Sign in with:</p>

                                    <div className='d-flex justify-content-between mx-auto' style={{width: '40%'}}>
                                        <MDBBtn tag='a' color='none' className='m-1' style={{color: '#1266f1'}}>
                                            <MDBIcon icon="facebook"/>
                                        </MDBBtn>

                                        <MDBBtn tag='a' color='none' className='m-1' style={{color: '#1266f1'}}>
                                            <MDBIcon icon='twitter' size="sm"/>
                                        </MDBBtn>

                                        <MDBBtn tag='a' color='none' className='m-1' style={{color: '#1266f1'}}>
                                            <MDBIcon icon='google' size="sm"/>
                                        </MDBBtn>

                                        <MDBBtn tag='a' color='none' className='m-1' style={{color: '#1266f1'}}>
                                            <MDBIcon icon='github' size="sm"/>
                                        </MDBBtn>
                                    </div>

                                    <p className="text-center mt-3">or:</p>
                                </div>

                                <MDBInput wrapperClass='mb-4' ref={userRef} aria-label='username' id='username'
                                          value={username || ''}
                                          required onChange={handleUsernameChange} type='text'/>
                                <MDBInput wrapperClass='mb-4' aria-label='password' id='password' value={password || ''}
                                          required onChange={handlePasswordChange} type='password'/>

                                <div className="d-flex justify-content-between mx-4 mb-4">
                                    <MDBCheckbox name='flexCheck' value='' id='flexCheckDefault' label='Remember me'/>
                                    <a href="!#">Forgot password?</a>
                                </div>

                                <MDBBtn className="mb-4 w-100" id="Register-btn" onClick={handleSignin}>Sign in</MDBBtn>
                                <p className="text-center">Not a member? <a href="#!">Register</a></p>

                            </MDBTabsPane>

                            <MDBTabsPane show={justifyActive === 'tab2'}>

                                <div className="text-center mb-3">
                                    <p>Sign up with:</p>

                                    <div className='d-flex justify-content-between mx-auto' style={{width: '40%'}}>
                                        <MDBBtn tag='a' color='none' className='m-1' style={{color: '#1266f1'}}>
                                            <MDBIcon icon='facebook' size="sm"/>
                                        </MDBBtn>

                                        <MDBBtn tag='a' color='none' className='m-1' style={{color: '#1266f1'}}>
                                            <MDBIcon icon='twitter' size="sm"/>
                                        </MDBBtn>

                                        <MDBBtn tag='a' color='none' className='m-1' style={{color: '#1266f1'}}>
                                            <MDBIcon icon='google' size="sm"/>
                                        </MDBBtn>

                                        <MDBBtn tag='a' color='none' className='m-1' style={{color: '#1266f1'}}>
                                            <MDBIcon icon='github' size="sm"/>
                                        </MDBBtn>
                                    </div>

                                    <p className="text-center mt-3">or:</p>
                                </div>

                                <MDBInput wrapperClass='mb-4' label='Firstname' id='firstname' type='text'
                                          value={firstname}
                                          onChange={handleFirstnameChange}/>
                                <MDBInput wrapperClass='mb-4' label='Lastname' id='lastname' type='text'
                                          value={lastname}
                                          onChange={handleLastnameChange}/>
                                <MDBInput wrapperClass='mb-4' label='Username' id='username' type='text'
                                          value={username}
                                          onChange={handleUsernameChange}/>
                                <MDBInput wrapperClass='mb-4' label='Email' id='email' type='email' value={email}
                                          onChange={handleEmailChange}/>
                                <MDBInput wrapperClass='mb-4' label='Password' id='password' type='password'
                                          value={password} onChange={handlePasswordChange}/>

                                <div className='d-flex justify-content-center mb-4'>
                                    <MDBCheckbox name='flexCheck' id='flexCheckDefault'
                                                 label='I have read and agree to the terms'/>
                                </div>

                                <MDBBtn className="mb-4 w-100" onClick={handleSignup}>Sign up</MDBBtn>

                            </MDBTabsPane>

                        </MDBTabsContent>
                    </MDBCardBody>
                </MDBCard>

            </MDBContainer>
            <Footer/>


        </div>
    );
}

export {LoginPage};