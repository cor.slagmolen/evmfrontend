import axios from 'axios';
import { BehaviorSubject} from 'rxjs';
const API_URL = 'http://localhost:8080/api/auth/';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));

class userService {

    get currentUserValue() {
        return currentUserSubject.value;
    }

}