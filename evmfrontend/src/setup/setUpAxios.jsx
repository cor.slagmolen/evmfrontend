import axios from 'axios';
import {timeout} from 'rxjs';
// Create instance called instance
const instance = axios.create({
    baseURL: 'https://localhost:8080/api/',
    headers: {
        'content-type':'application/octet-stream',
        'x-rapidapi-host':'example.com',
        'x-rapidapi-key': process.env.RAPIDAPI_KEY
    },
});
export default {
    getData: (path) =>
        instance({
            'method':'GET',
            'url': path,
            'params': {
                'search':'parameter',
            },
        }),
    postData: (path) =>
        instance({
            'method': 'POST',
            'url': path,
            'data': {
                'item1':'data1',
                'item2':'item2'
            },
            'headers': {
                'content-type':'application/json'  // override instance defaults
            }
        })
};