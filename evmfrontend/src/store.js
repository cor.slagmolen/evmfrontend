
import {configureStore} from '@reduxjs/toolkit';
import authReducer from './features/user/store/UserSlice';
import teamReducer from './features/team/store/TeamSlice';
import teamRoleReducer from './features/role/store/TeamRoleSlice';
import storage from 'redux-persist/lib/storage';
import {persistReducer, persistStore} from 'redux-persist';

const persistConfig = {
    key: 'root',
    storage
};

const authPersistReducer = persistReducer(persistConfig, authReducer);
const teamPersistReducer = persistReducer(persistConfig, teamReducer);
const teamRolePersistReducer = persistReducer(persistConfig, teamRoleReducer);
export const store = configureStore({
    reducer: {
        auth: authPersistReducer,
        team: teamPersistReducer,
        teamRole: teamRolePersistReducer
    }
});
export const persistor = persistStore(store);

